#define WINVER 0X500
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <sstream>

using namespace std;

// Globals Variables

HANDLE clip;

// Class:
class Mouse
{
    INPUT ip;
public:
    Mouse()
    {
        ip.type = INPUT_MOUSE;
    }

    void MoveTo(int x, int y)
    {
        SetCursorPos(x, y);
    }

    void LeftClick()
    {
        ip.mi = {0, 0, 0, 0x2, 0, NULL};
        SendInput(1, &ip, sizeof(ip));
        ip.mi = {0, 0, 0, 0x4, 0, NULL};
        SendInput(1, &ip, sizeof(ip));
    }
};

class Keyboard
{
    INPUT ip;
public:
    Keyboard()
    {
        ip.type = INPUT_KEYBOARD;
    }

    void PressKey(unsigned int vkCode)
    {
        ip.ki = {vkCode, 0, 0, 0, NULL};
        SendInput(1, &ip, sizeof(ip));
    }

    void ReleaseKey(unsigned int vkCode)
    {
        ip.ki = {vkCode, 0, KEYEVENTF_KEYUP, 0, NULL};
        SendInput(1, &ip, sizeof(ip));
    }

    void SendChar(char c)
    {
        ip.ki = {c, 0, 0, 0, NULL};
        SendInput(1, &ip, sizeof(ip));
        ip.ki = {c, 0, KEYEVENTF_KEYUP, 0, NULL};
        SendInput(1, &ip, sizeof(ip));
    }
};

// Functions:

string GetClipboardText()
{
  // Try opening the clipboard
  if (! OpenClipboard(nullptr))
    cout<<"Can't open CB";

  // Get handle of clipboard object for ANSI text
  HANDLE hData = GetClipboardData(CF_TEXT);
  if (hData == nullptr)
    cout<<"Can't handle of CB";

  // Lock the handle to get the actual text pointer
  char * pszText = static_cast<char*>( GlobalLock(hData) );
  if (pszText == nullptr)
    cout<<"Can't get char of Handle CB";

  // Save text in a string class instance
  string text( pszText );

  // Release the lock
  GlobalUnlock( hData );

  // Release the clipboard
  CloseClipboard();

  return text;
}
void Copy()
{
    Keyboard kb;

    kb.PressKey(VK_CONTROL);
    kb.SendChar('A');
    kb.ReleaseKey(VK_CONTROL);
    Sleep(100);
    kb.PressKey(VK_CONTROL);
    kb.SendChar('C');
    kb.ReleaseKey(VK_CONTROL);
}

void Point2Operation()
{
    Mouse ms;

    ms.MoveTo(404 , 343);
    ms.LeftClick();

    Copy();
}

VOID Point2Result()
{
    Mouse ms;
    ms.MoveTo(400 , 407);
    ms.LeftClick();

    Copy();
}

bool Check(string op, string re)
{
    int i = 0;
    while (i<op.length() && op[i] != '+' && op[i] != '-' && op[i] != '*' && op[i] != '/' )
    {
        i++;
    }

    string sa = op.substr(0,i-1);
    string sb = op.substr(i+2,op.length());
    string sc = re.substr(1,re.length());

    //cout<<sa<<" "<<sb<<" "<<sc<<endl;

    stringstream ss1(sa);
    stringstream ss2(sb);
    stringstream ss3(sc);

    int a, b, c;
    ss1 >> a;
    ss2 >> b;
    ss3 >> c;

    switch (op[i])
    {
    case '+':
        if (a+b == c) return true;
        break;
    case '-':
        if (a-b == c) return true;
        break;
    case '*':
        if (a*b == c) return true;
        break;
    case '/':
        if (a/b == c) return true;
        break;
    }

    return false;
}

int main()
{
    cout<<"";
    cout<<"Input 'ms' to START"<<endl;
    string s;
    cin>>s;
    cout<<"MathSky hello you :)"<<endl;
    int score=0;
    cout<<"Ban muon bao nhieu diem? ";
    cin>> score;

    string operation = "";
    string result = "";

    Mouse ms;

    for(int i = 0; i< score; i++)
    {
        Point2Operation();
        Sleep(50);
        operation = GetClipboardText();
        Sleep(50);
        Point2Result();
        Sleep(50);
        result = GetClipboardText();
        Sleep(100);
        if (Check(operation, result))
        {
            ms.MoveTo(321, 631);
            ms.LeftClick();
        }
        else
        {
            ms.MoveTo(469 , 627);
            ms.LeftClick();
        }

        Sleep(100);
    }
    return 0;
}


